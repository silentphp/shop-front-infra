import Vue from 'vue'
import App from './App.vue'

import Router from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'

import User from "./components/admin/User";
import UserEdit from "./components/admin/UserEdit";
import Product from "./components/admin/Product";
import ProductAdd from "./components/admin/ProductAdd";
import ProductEdit from "./components/admin/ProductEdit";
import Environment from "./components/admin/Environment";
import EnvironmentAdd from "./components/admin/EnvironmentAdd";
import Server from "./components/admin/Server";
import ServerAdd from "./components/admin/ServerAdd";
import EnvironmentEdit from "./components/admin/EnvironmentEdit";
import ServerEdit from "./components/admin/ServerEdit";
import SelectConf from "./components/SelectConf";
import Complet from "./components/Complet";
import Order from "./components/admin/Order";
import Auth from "./components/Auth";
import Main from "./components/Main";

import './components/style.css'

import axios from 'axios'
import OrderDetail from "./components/admin/OrderDetail";
import MyOrder from "./components/MyOrder";
import MyOrderDetail from "./components/MyOrderDetail";

import VueFilterDateFormat from 'vue-filter-date-format';
import Test from "./components/Test";
import Sla from "./components/admin/Sla";
import SlaAdd from "./components/admin/SlaAdd";
import SlaEdit from "./components/admin/SlaEdit";
import OrderRejected from "./components/admin/OrderRejected";
import Cart from "./components/Cart";
import CartDetail from "./components/CartDetail";
import CartData from "./components/CartData";

import Vue2Filters from 'vue2-filters'
Vue.use(Vue2Filters)

Vue.use(VueFilterDateFormat);

axios.defaults.baseURL = process.env.ROOT_API;
//axios.defaults.baseURL = 'http://marketplace001.x5.ru:8080';
Vue.prototype.$http = axios;

axios.defaults.config = {
  headers: {
    Authorization: localStorage.getItem('token'),
  }
};

import Vuelidate from 'vuelidate';
import OperationSystem from "./components/admin/OperationSystem";
import OperationSystemAdd from "./components/admin/OperationSystemAdd";
import OperationSystemEdit from "./components/admin/OperationSystemEdit";
import OrderData from "./components/OrderData";
Vue.use(Vuelidate);

Vue.use(BootstrapVue);
Vue.use(Router);


const router = new Router({
  routes: [
    {
      path: '/',
      component: Main,
      props: true,
    },
    {
      path: '/admin/user',
      component: User,
      props: true,
    },
    {
      path: '/admin/user-edit/:id',
      component: UserEdit,
      name: 'user-edit',
      props: true,
    },
    {
      path: '/admin/product',
      component: Product,
      props: true,
    },
    {
      path: '/admin/product-add',
      component: ProductAdd,
      name: 'product-add',
      props: true,
    },
    {
      path: '/admin/product-edit',
      component: ProductEdit,
      name: 'product-edit',
      props: true,
    },
    {
      path: '/admin/env',
      component: Environment,
      props: true,
    },
    {
      path: '/admin/env-add',
      component: EnvironmentAdd,
      name: 'env-add',
      props: true,
    },
    {
      path: '/admin/env-edit',
      component: EnvironmentEdit,
      name: 'env-edit',
      props: true,
    },
    {
      path: '/admin/os',
      component: OperationSystem,
      props: true,
    },
    {
      path: '/admin/os-add',
      component: OperationSystemAdd,
      name: 'os-add',
      props: true,
    },
    {
      path: '/admin/os-edit',
      component: OperationSystemEdit,
      name: 'os-edit',
      props: true,
    },
    {
      path: '/admin/sla',
      component: Sla,
      props: true,
    },
    {
      path: '/admin/sla-add',
      component: SlaAdd,
      name: 'sla-add',
      props: true,
    },
    {
      path: '/admin/sla-edit',
      component: SlaEdit,
      name: 'sla-edit',
      props: true,
    },
    {
      path: '/admin/server',
      component: Server,
      props: true,
    },
    {
      path: '/admin/server-add',
      component: ServerAdd,
      name: 'server-add',
      props: true,
    },
    {
      path: '/admin/server-edit',
      component: ServerEdit,
      name: 'server-edit',
      props: true,
    },
    {
      path: '/admin/order',
      component: Order,
      props: true,
    },
    {
      path: '/admin/order/:id',
      component: OrderDetail,
      name: 'order-detail',
      props: true,
    },
    {
      path: '/admin/order_rejected/:id',
      component: OrderRejected,
      props: true,
    },
    {
      path: '/select_conf',
      component: SelectConf,
      props: true,
    },
    {
      path: '/cart',
      component: Cart,
      props: true,
    },
    {
      path: '/cart_data',
      component: CartData,
      props: true,
    },
    {
      path: '/order_data/:id',
      component: OrderData,
      props: true,
    },
    {
      path: '/completed',
      component: Complet,
      props: true,
    },
    {
      path: '/my_order',
      component: MyOrder,
      props: true,
    },
    {
      path: '/my_order/:id',
      component: MyOrderDetail,
      name: 'my-order-detail',
      props: true,
    },
    {
      path: '/cart_detail',
      component: CartDetail,
      props: true,
    },
    {
      path: '/auth',
      component: Auth,
      props: true,
    },
    {
      path: '/test',
      component: Test,
      props: true,
    },
  /*  {
      path: '/main',
      component: Main,
      props: true,
    },*/
  ]
});

new Vue({
  el: '#app',
  render: h => h(App),
  router
})
